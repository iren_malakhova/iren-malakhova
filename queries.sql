drop table if exists author_books;
drop table if exists author;
drop table if exists book;

CREATE TABLE author_books (
    author_id INTEGER, 
    book_id INTEGER, 
    FOREIGN KEY(author_id) REFERENCES author (author_id), 
    FOREIGN KEY(book_id) REFERENCES book (book_id)
)

CREATE TABLE author (
    author_id INTEGER NOT NULL, 
    author_name VARCHAR(80), 
    PRIMARY KEY (author_id), 
    UNIQUE (author_name)
)

CREATE TABLE book (
    book_id INTEGER NOT NULL, 
    book_title VARCHAR(80), 
    PRIMARY KEY (book_id)
)

insert into author (athor_name) values 
('1','Dashvar L.'),
('2','Tuturkevich L.'),
('3','Danilevich L.'),
('4','Luts M.'),
('5','Beazly D.')

insert into author (athor_name) values 
('1','Marck'),
('2','Gotsyk'),
('3','Groom'),
('4','Python'),
('5','Python: Quick tutorial')

insert into author (athor_name) values 
('1','1'),
('1','2'),
('2','3'),
('3','4'),
('4','5')

      
