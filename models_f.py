# -*- coding: utf-8 -*-

import os
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash
     

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////{}'.format(os.path.abspath("books.db"))
app.config['SECRET_KEY']='development key'
app.config['USERNAME']='admin'
app.config['PASSWORD']='default'

db = SQLAlchemy(app)

'''make association table for Author and Book'''
author_books = db.Table('author_books', db.Model.metadata,
    db.Column('author_id', db.Integer, db.ForeignKey('author.author_id')),
    db.Column('book_id', db.Integer, db.ForeignKey('book.book_id'))
)

class Author(db.Model):
    __tablename__ = 'author'
    author_id = db.Column(db.Integer, primary_key=True)
    author_name = db.Column(db.String(80))
    
    books = db.relationship("Book",
                    secondary=author_books,
                    backref="author_names")

    def __init__(self, author_name):
        self.author_name = author_name

    def __repr__(self):
        return '<Author %r>' % self.author_name
        
class Book(db.Model):
    __tablename__ = 'book'
    book_id = db.Column(db.Integer, primary_key=True)
    book_title = db.Column(db.String(80))
    
    authors = db.relationship("Author",
                    secondary=author_books,
                    backref="allbooks")

    def __init__(self, book_title):
        self.book_title = book_title

    def __repr__(self):
        return '<Book %r>' % self.book_title
        


@app.route('/')
def show_entries():
    '''method shows all books in library: list all authors with its books'''
    book_list = {}
    for author in db.session.query(Author).order_by(Author.author_name).all():  
        book_list[author.author_name] = author
        
    return render_template('show_entries.html', entries=book_list)
    


def check_valid(author_name, book_title):
    '''function check that author_name, book_title are  correct'''
    if author_name.isalpha() or  book_title.isalpha():
        return True
        


@app.route('/add', methods=['POST'])
def add_entry():
    '''method adding new book in base'''
    book_add = False   
    author_id = None
    author_books = []
    
    if not session.get('logged_in'):
        abort(401)
    db.create_all()
    author_name = request.form['ath']
    book_title = request.form['book']
    
    if not check_valid(author_name, book_title):
        flash('Some filds are incorrect')
        return redirect(url_for('show_entries'))

       
    a = Author(author_name)
    b = Book(book_title)
    
    author_in_base = False
    
    '''check author'''
    for author in db.session.query(Author).filter(Author.author_name == author_name): 
        if author.author_name !=  author_name:
            db.session.add(a)
        else:
            author_in_base = True
            author_id = author.author_id
            author_books = author.books
        
        
    '''check book'''    
    book_in_base = False
    for book in db.session.query(Book).filter(Book.book_title == book_title):
        if book.book_title ==  book_title:
            book_in_base = True
            
    if book_in_base and author_in_base:
        flash('This book already there is in library')
    else:
        db.session.add(b)
        book_add = True
        
    '''add ralationships'''
    if author_in_base == False and book_add == True:
        a.books.append(b)
        b.authors.append(a)
    elif author_in_base == True and book_add == True:
        for author in db.session.query(Author).filter(Author.author_id == author_id):
           author.books.append(b) 
       
    db.session.commit()
    if book_add == True:
        flash('New entry was successfully posted')
    return redirect(url_for('show_entries'))
    
@app.route('/delete_book/<int:book_id>')
def del_book(book_id):
    if not session.get('logged_in'):
        abort(401)
    db.create_all()
    
    if not book_id:
        abort(401)
    
    db.session.query(Book).filter(Book.book_id == book_id).delete()
    db.session.commit()
    return redirect(url_for('show_entries'))
    
    
@app.route('/delete_author/<int:author_id>')
def delete_author(author_id):
    '''not work correctly. it need cascad delet'''
    if not session.get('logged_in'):
        abort(401)
    db.create_all()
    
    if not author_id:
        abort(401)
    
    author = db.session.query(Author).filter(Author.author_id == author_id).one()
    for book in author.books:
        db.session.query(Book).filter(Book.book_id == book.book_id)
       db.session.query(Author).filter(Author.author_id == author_id).delete()
    
    #db.session.query(author_books).filter(author_books.c.author_id == author_id).delete() 
        
    db.session.commit()
    return redirect(url_for('show_entries'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)
    
@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))
    
if __name__ == '__main__':            
    app.debug = True
    app.run()

        


